'use strict';

describe('Controller: CampaignDesignCtrl', function () {

  // load the controller's module
  beforeEach(module('csApp'));

  var CampaignDesignCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CampaignDesignCtrl = $controller('CampaignDesignCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of configuration options to the scope', function () {
    expect(scope.campaignDesignConfigOptions.length).toBe(3);
  });
});
