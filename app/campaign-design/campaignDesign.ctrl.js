'use strict';

/**
 * @ngdoc function
 * @name smartfocusWebApp.controller:CampaignDesignCtrl
 * @description
 * # DesignerCtrl
 * Controller of the smartfocusWebApp
 */
angular.module('ctrl.campaignDesign', [])
  .controller('CampaignDesignCtrl', function ($scope) {
    $scope.campaignDesignConfigOptions = [{
      'configOption': 'Algorithm',
      'configOptionInfo': 'Select offer algorithm.'
    },{
      'configOption': 'Properties',
      'configOptionInfo': 'Select offer properties.'
    },{
      'configOption': 'Drivers',
      'configOptionInfo': 'Select offer drivers.'
    }];

    $scope.onDrop = function (data, event) {
      var customObjectData = data['json/offer-item'];
      console.log('dropped custom object');
      console.log('data: ' + JSON.stringify(data));
    };

    // Drag over handler.
    $scope.onDragOver = function (event) {
      console.log('dragged over');
    };
  });
