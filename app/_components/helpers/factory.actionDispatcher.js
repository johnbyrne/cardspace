'use strict';

/**
 * @ngdoc function
 * @name app/_components/helpers/factory.actionDispatcher.js
 * @description
 * # ActionDispatcher
 * Factory of the smartfocusWebApp
 */
angular.module('csApp')
  .factory('ActionDispatcher', function($rootScope) {
    var actionDispatcherService = {};

    actionDispatcherService.message = '';

    actionDispatcherService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.dispatchAction();
    };

    actionDispatcherService.dispatchAction = function() {
        $rootScope.$broadcast('dispatchAction');
    };

    return actionDispatcherService;
});
