'use strict';

/**
 * @ngdoc function
 * @name csApp.controller:CampaignDesignCtrl
 * @description
 * # DesignerCtrl
 * Controller of the smartfocusWebApp
 */
angular.module('cs.home', [])
  .controller('HomeCtrl', function ($scope) {
    $scope.users = JSON.parse(localStorage.getItem('users'));
  });
