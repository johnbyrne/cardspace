'use strict';

/**
 * @ngdoc overview
 * @name smartfocusWebApp
 * @description
 * # smartfocusWebApp
 *
 * Main module of the application.
 */
angular
  .module('csApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'sf.directives',
    'cs.home'
  ])

.run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {

    // It's very handy to add references to $state and $stateParams to the $rootScope
    // so that you can access them from any scope within your applications.For example,
    // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
    // to active whenever 'contacts.list' or one of its decendents is active.
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    }
  ]
)

.config([
  '$stateProvider', '$urlRouterProvider', '$locationProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider) {

    localStorage.clear();

    localStorage.setItem('users', JSON.stringify([
      {'name':'John Smith', 'email':'test1@test.com', "password":"test1234"},
      {'name':'James Smith', 'email':'test1@test.com', "password":"test1243"},
      {'name':'Joe Bloggs', 'email':'test1@test.com', "password":"test1234"},
      {'name':'Jane Doe', 'email':'test1@test.com', "password":"test1234"}
    ]));

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('/', {
      url: '/',
        views: {
          'main-view': {
              templateUrl: './home/' +
                'home.tpl.html',
              controller: 'HomeCtrl'
          }
        }
    })

    .state('segmentation', {
      url: '/segmentation',
        views: {
          'main-view': {
              templateUrl: './segmentation/' +
                'segmentation.tpl.html'
          }
        }
    })

    .state('campaign-design', {
      url: '/campaign-dashboard',
        views: {
          'main-view': {
              templateUrl: './campaign-design/' +
                'campaign-design.tpl.html',
              controller: 'CampaignDesignCtrl'
          }
        }
    })

    .state('campaign-execution', {
      url: '/campaign-execution',
        views: {
          'main-view': {
              templateUrl: './campaign-execution/' +
                'campaign-execution.tpl.html'
          }
        }
    });

    //$locationProvider.html5Mode(true);
  }
]);
